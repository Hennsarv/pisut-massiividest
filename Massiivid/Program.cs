﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {
            // massiiv
 
            int[] m = new int[10]; // massiiv 10 elemendiga
                                   // elemendid nummerdatakse alates 0-st

            m = new int[] { 1,3,6,2,4,8,5,7};

            // kolm viisi initsialiseerida
            string[] sõidukid1 = new string[4]  { "autod", "laevad", "Lennukid", "Jalgrattad" };
            string[] sõidukid2 = new string[]   { "autod", "laevad", "Lennukid", "Jalgrattad" };
            string[] sõidukid3 =                { "autod", "laevad", "Lennukid", "Jalgrattad" };

            Array.Sort(m);
            for (int k = 0; k < m.Length; k++)
                Console.WriteLine($"massiivi {k} element on {m[k]}");

            Console.WriteLine( m.Sum()  );

            Array.Sort(sõidukid1);

            foreach (string s in sõidukid1.Reverse()) Console.WriteLine(s);

            // massiiv mis koosneb omakorda massiividest
            int[][] veider = {new int[] { 1, 2 }, new int[] { 1, 2, 3 }, new int[] { 1, 2, 3, 4 } };

            foreach (int[] mx in veider)
            {
                
                foreach (int i in mx)
                {
                    Console.Write($"{i} " );
                }
                Console.WriteLine();
            }

            // kahemõõtmeline massiiv
            int[,] veelVeidram = { { 1, 2, 3, 0 }, { 4, 5, 6, 0 }, { 7, 8, 9, 0 } , { 1, 2, 3, 4 } };

            foreach (int i in veelVeidram) Console.WriteLine(i);

            for (int i = 0; i < veelVeidram.GetLength(0); i++)
            {
                for (int j = 0; j < veelVeidram.GetLength(1); j++)
                {
                    Console.Write($"{veelVeidram[i,j]} " );
                }
                Console.WriteLine();
            }

           

        }
    }

}
